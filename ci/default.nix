{ sources ? import ./nix/sources.nix, nixpkgs ? (import sources.nixpkgs.outPath {}) }:

with nixpkgs;
let
  haskellPackages_ = nixpkgs.haskell.packages.ghc963.override {
       all-cabal-hashes = sources.all-cabal-hashes.outPath;
       overrides = self: super: {
         mkDerivation = args: super.mkDerivation (args // {
           enableLibraryProfiling = false;
           doCheck = false;
           doHoogle = false;
           doHaddock = false;
         });
       };
     };

  haskellPackages = haskellPackages_.extend (self: super: {
    # This is marked broken in nixpkgs, but seems to work fine.
    cabal-plan = self.callHackage "cabal-plan" "0.7.3.0" {};
    # The easiest fix for an incorrect override in nixpkgs (https://github.com/NixOS/nixpkgs/issues/260013)
    tls        = self.callHackage "tls" "1.6.0" {};
  });


  hackage-repo-tool =
    let src = sources.hackage-security.outPath;
    in nixpkgs.haskell.lib.doJailbreak (haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {});

  overlay-tool =
    let src = sources.overlay-tool;
    in nixpkgs.haskell.lib.doJailbreak (haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src { });

  head-hackage-ci =
    let
      src = nixpkgs.nix-gitignore.gitignoreSource [] ./.;
    in haskellPackages.callCabal2nix "head-hackage-ci" src {};

  buildDeps = import ./build-deps.nix { pkgs = nixpkgs; };

  buildDepsFragment =
    let

      mkCabalFragment = pkgName: deps:
        with pkgs.lib;
        let
          libDirs = concatStringsSep " " (map (dep: getOutput "lib" dep + "/lib") deps);
          includeDirs = concatStringsSep " " (map (dep: getOutput "dev" dep + "/include") deps);
        in ''
        package ${pkgName}
          extra-lib-dirs: ${libDirs}
          extra-include-dirs: ${includeDirs}
        '';
    in
      pkgs.lib.concatStringsSep "\n"
        (pkgs.lib.mapAttrsToList mkCabalFragment buildDeps);

  buildDepsFile = pkgs.writeText "deps.cabal.project" buildDepsFragment;

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        haskellPackages.cabal-install haskellPackages.ghc gcc binutils-unwrapped pwgen gnused
        hackage-repo-tool overlay-tool python3 jq pkg-config
        git # cabal-install wants this to fetch source-repository-packages
      ];

      pkg_config_depends = lib.makeSearchPathOutput "dev" "lib/pkgconfig" (lib.concatLists (lib.attrValues buildDeps));

    in
      runCommand "repo" {
        nativeBuildInputs = [ makeWrapper ];
      } ''
        mkdir -p $out/bin
        makeWrapper ${head-hackage-ci}/bin/head-hackage-ci $out/bin/head-hackage-ci \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${./discover_tarball.sh} $out/bin/discover_tarball.sh \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${../run-ci} $out/bin/run-ci \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin \
            --prefix PKG_CONFIG_PATH : ${pkg_config_depends} \
            --set USE_NIX 1 \
            --set CI_CONFIG ${./config.sh}

        makeWrapper ${./find-job.sh} $out/bin/find-job \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${./find-latest-job.sh} $out/bin/find-latest-job \
            --prefix PATH : ${lib.makeBinPath deps}:$out/bin

        makeWrapper ${xz}/bin/xz $out/bin/xz
        makeWrapper ${curl}/bin/curl $out/bin/curl
      '';
in
  mkShell { 
    name = "head-hackage-build-env"; 
    buildInputs = [ build-repo ];   
    cabalDepsSrc = buildDepsFragment;
  }
