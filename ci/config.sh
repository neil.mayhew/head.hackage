# vi: set filetype=sh

# Packages expected not to build due to GHC bugs. This is `source`'d by the CI
# script and the arguments in BROKEN_ARGS are added to the hackage-ci
# command-line.

# Mark the named package as broken.
#
# Usage:
#    broken $pkg_name $ghc_ticket_number
#
function broken() {
  pkg_name="$1"
  ticket="$2"
  echo "Marking $pkg_name as broken due to #$ticket"
  EXTRA_OPTS="$EXTRA_OPTS --expect-broken=$pkg_name"
}

function only_package() {
  echo "Adding $@ to --only package list"
  for pkg in $@; do
    EXTRA_OPTS="$EXTRA_OPTS --only=$pkg"
  done
}

function test_package() {
  echo "Adding $@ to --test-package list"
  EXTRA_OPTS="$EXTRA_OPTS --test-package=$1=$2"
}

# Return the version number of the most recent release of the given package
function latest_version() {
  pkg=$1
  curl -s -H "Accept: application/json" -L -X GET http://hackage.haskell.org/package/$pkg/preferred | jq '.["normal-version"] | .[0]' -r
}

# Add a package to the set of packages that lack patches but are nevertheless
# tested.
function extra_package() {
  pkg_name="$1"
  version="$2"
  if [ -z "$version" ]; then
    version=$(latest_version $pkg_name)
  fi
  echo "Adding $pkg_name-$version to extra package set"
  EXTRA_OPTS="$EXTRA_OPTS --extra-package=$pkg_name==$version"
}

# Mark a package to be declared with build-tool-depends, not build-depends.
# This is necessary for packages that do not have a library component.
function build_tool_package() {
  pkg_name="$1"
  echo "Adding $pkg_name as a build-tool package"
  EXTRA_OPTS="$EXTRA_OPTS --build-tool-package=$pkg_name"
}

if [ -z "$GHC" ]; then GHC=ghc; fi

function ghc_version() {
  $GHC --version | sed 's/.*version \([0-9]*\.\([0-9]*\.\)*\)/\1/'
}

function ghc_commit() {
  $GHC --print-project-git-commit-id
}

function ghc_arch() {
  $GHC --print-host-platform
}

# ======================================================================
# Baseline constraints
#
# These constraints are applied to preclude the solver from producing build
# plans using ancient, under-constrained package versions.

EXTRA_OPTS="$EXTRA_OPTS --extra-cabal-fragment=$(pwd)/config.cabal.project"

# ======================================================================
# The lists begin here
#
# For instance:
#
#    broken "lens" 17988

version="$(ghc_version)"
commit="$(ghc_commit)"
arch="$(ghc_arch)"
echo "Found GHC $version, commit $commit."
case $version in
  9.4.*)
    #       package                   ticket
    broken  linear-generics           22546
    broken  liquidhaskell-boot        350
    ;;

  9.6.*)
    #       package                   ticket
    broken  liquidhaskell-boot        350
    ;;

  9.8.*)
    #       package                   ticket
    broken  liquidhaskell-boot        350
    ;;

  9.9.*)
    #       package                   ticket
    ;;

  *)
    echo "No broken packages for GHC $version"
    ;;
esac

case $arch in
  x86_64-*-*)
  #       package                   ticket
  ;;
  aarch64-*-*)
  # These just don't build on aarch64
  #       package                   ticket
  broken  charsetdetect             00000
  broken  packman                   00000
  ;;
  *)
  echo "$arch is unknown to head.hackage, assuming nothing is broken."
  ;;
esac



# Extra packages
# ==============
#
# These are packages which we don't have patches for but want to test anyways.
extra_package lens 5.2.3
extra_package optics 0.4.2.1
extra_package aeson 2.2.1.0
extra_package criterion 1.6.3.0
extra_package scotty 0.21
extra_package generic-lens 2.2.2.0
extra_package microstache 1.0.2.3
extra_package singletons-base 3.1.1
extra_package servant 0.20.1
extra_package hgmp 0.1.2.1
extra_package Agda 2.6.4.1
extra_package mmark 0.0.7.6
extra_package doctest 0.22.2
extra_package tasty 1.5
extra_package pandoc 3.1.11.1
extra_package servant-conduit 0.16
extra_package servant-machines 0.16
extra_package linear-generics 0.2.3
extra_package futhark 0.25.13
extra_package generic-random 1.5.0.1
extra_package lame 0.2.2
extra_package inspection-testing 0.5.0.3
extra_package ghcide 2.6.0.0
extra_package ghc-typelits-extra 0.4.6

# This package is affected by https://gitlab.haskell.org/ghc/ghc/-/issues/22912
extra_package vector-space 0.16

# Build-tool packages
build_tool_package alex
build_tool_package happy
build_tool_package c2hs

# $BUILD_MODE controls how head.hackage runs.
# ===========================================
#
# Four build modes exist: FULL, QUICK, TEST, and COMPAT.
#
# FULL.
# ------
# Build all patched + extra packages.
#
# QUICK.
# ------
# Build the "quick" configuration, which builds a small subset of the overall
# package set. (Also runs tests!) We do this during the merge request validation
# pipeline. Note: If "$QUICK" is non-null, it is used as a backwards-compat
# synonym for BUILD_MODE=QUICK.
#
# TEST.
# -----
# Just build the local test packages and run the tests.
#
# COMPAT: FULL + TEST.
# --------------------
# Backwards-compat default build mode.
#
: ${BUILD_MODE:=COMPAT}
if [ -n "$QUICK" ]; then
    BUILD_MODE=QUICK
fi
case "$BUILD_MODE" in
    FULL) ;;
    QUICK)
        only_package tasty
        only_package Cabal
        only_package microlens
        only_package free
        only_package optparse-applicative
        test_package system-test "$(pwd)/../tests/ghc-debug/**/*.cabal"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        ;;
    TEST)
        # FIXME: I specify a single "only_package" to prevent all the other
        # packages from being built. Morally, I really want to say "build
        # nothing at all besides the tests".
        only_package tasty
        test_package system-test "$(pwd)/../tests/ghc-debug/**/*.cabal"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        test_package all "$(pwd)/../tests/text"
        test_package bytestring-tests "$(pwd)/../tests/bytestring"
        test_package all "$(pwd)/../tests/containers/containers-tests"
        case $version in
          9.4.*)
            ;;
          9.6.*)
            ;;
          9.8.*)
            ;;
          9.9.*)
            test_package liquidhaskell-boot "$(pwd)/../tests/liquidhaskell/liquidhaskell-boot"
            ;;
          *)
            ;;
        esac
        ;;
    COMPAT)
        test_package system-test "$(pwd)/../tests/ghc-debug/**/*.cabal"
        test_package ghc-tests "$(pwd)/../tests/ghc-tests"
        case $version in
          9.4.*)
            ;;
          9.6.*)
            ;;
          9.8.*)
            ;;
          9.9.*)
            test_package liquidhaskell-boot "$(pwd)/../tests/liquidhaskell/liquidhaskell-boot"
            ;;
          *)
            ;;
        esac
        ;;
esac
