#!/bin/sh

PIPELINE_TYPE=validation

if [ -n "$PIPELINE_OVERRIDE"  ]; then
    PIPELINE_TYPE="$PIPELINE_OVERRIDE"
# Triggered by GHC
elif [ "$CI_PIPELINE_SOURCE" = "pipeline" -a '(' -n "$UPSTREAM_COMMIT_SHA" -o -n "$UPSTREAM_PIPELINE_ID" ')' ]; then
    PIPELINE_TYPE=downstream
# Nightly repo update, to be scheduled
#
# SCHEDULE_TYPE must be set when creating the scheduled job. It is used to
# explicitly identify which schedule we want.
elif [ "$CI_PIPELINE_SOURCE" = "schedule" -a "$SCHEDULE_TYPE" = "update-repo" ]; then
    PIPELINE_TYPE=update-repo
fi

mk_pipeline () {
    echo "Generating $PIPELINE_TYPE pipeline"
    cp -v $1 gitlab-generated-pipeline.yml
}

case "$PIPELINE_TYPE" in
    validation)
        mk_pipeline ci/pipelines/validation.yml
        ;;
    downstream)
        mk_pipeline ci/pipelines/downstream.yml
        ;;
    update-repo)
        mk_pipeline ci/pipelines/update-repo.yml
        ;;
    update-branch)
        mk_pipeline ci/pipelines/update-branch.yml
        ;;
    *)
        echo "Unknown pipeline type: $PIPELINE_TYPE"
        exit 1
        ;;
esac
