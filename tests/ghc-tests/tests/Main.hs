{-# LANGUAGE NumericUnderscores #-}
module Main where

import System.Exit

import Chan001
import MVar001
import qualified HClose003
import qualified Concio002
import qualified Rand001
import qualified HWaitPipe
import qualified HWaitSocket
import qualified Cgrun068
import qualified T7953
import qualified CopySmallArrayStressTest
import qualified T367
import qualified T367A
import qualified Throwto001
import qualified PerformGC
import qualified Conc023
import qualified CompareAndSwap
import qualified T13916
import qualified T8138
import qualified Drvrun022
import qualified T3087
import qualified FFI009
import qualified T2267
import qualified T14768
import qualified T15038.Main
import qualified TC191
import qualified TC220
import qualified T12926
import qualified T14854
import qualified T15038.Main as T15038
import qualified MaessenHashtab.HashTest as HashTest
import Arr016

import Test.Tasty
import Test.Tasty.HUnit
import Tasty.Bronze
import qualified Test.Tasty.Silver.Interactive as I

bronzeDir = "bronze/"

main :: IO ()
main = defaultMainWithIngredients (I.interactiveTests (const False) : defaultIngredients) $
        testGroup "all" [ testChan
                        , testMVar
                        , arr016
                        , goldenVsOut "concio002" "bronze/concio002.out" Concio002.main
                        , goldenVsOut "rand001" "bronze/rand001.out" Rand001.main
                        , goldenVsOut "cgrun068" "bronze/cgrun068.out" (Cgrun068.main 100)
                        , goldenVsOut "CopySmallArrayStressTest" "bronze/CopySmallArrayStressTest.out" (CopySmallArrayStressTest.main 100)
                        , goldenVsOut "T7953" "bronze/T7953.out" T7953.main
                        , goldenVsOut "CompareAndSwap" "bronze/CompareAndSwap.out" CompareAndSwap.main
                        , goldenVsOut "T8138" "bronze/T8138.out" T8138.main
                        , goldenVsOut "Drvrun022" "bronze/Drvrun022.out" Drvrun022.main
                        , goldenVsOut "T3087" "bronze/T3087.out" T3087.main
                        , goldenVsOut "FFI009" "bronze/FFI009.out" FFI009.main
                        , HashTest.hashTest
                        , goldenVsOut "T14768" "bronze/T14768.out" T14768.main
                        , goldenVsOut "T14854" "bronze/T14854.out" T14854.main
                        , goldenVsOut "T14854" "bronze/T15038.out" T15038.main

                        , testCase "Throwto001" (Throwto001.main 1000 2000)
                        , testCase "PerformGC" (PerformGC.main 400)
                        , testCase "Conc023" Conc023.main
                        , testCase "T13916" T13916.main

                        -- This test can cause hangs on CI (I think)
                        -- , goldenVsOut "T367" "bronze/T367.out" T367.main
                        -- I think this causes a hang on CI
                        -- , localOption (mkTimeout 2_000_000) $ goldenVsOut "T367A" "bronze/T367A.out" T367A.main

                        -- These are flaky
                        -- , goldenVsOut "hClose003" "bronze/hClose003.out" HClose003.main
                        -- , goldenVsOut "hwaitPipe" "bronze/hwaitPipe.out" HWaitPipe.main
                        -- , goldenVsOut "hwaitSocket" "bronze/hwaitSocket.out" HWaitSocket.main
                        ]
