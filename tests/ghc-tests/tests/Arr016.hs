{-# LANGUAGE ScopedTypeVariables #-}

module Arr016 where

{-
 - This is a test framework for Arrays, using QuickCheck
 -
 -}

import qualified Data.Array as Array
import Control.Monad ( liftM2, liftM3, liftM4 )
import System.Random
import Test.Tasty
import Test.Tasty.QuickCheck


import Data.Ix
import Data.List( (\\) )

infixl 9  !, //

prop_array =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->
    Array.array b vs
         `same_arr`
    array b vs
prop_listArray =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (vector (length [fst b..snd b]))
                           $ \ (vs :: [Bool]) ->
    Array.listArray b vs == Array.array b (zipWith (\ a b -> (a,b))
                                                   (Array.range b) vs)

prop_indices =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->
    let arr = Array.array b vs
    in Array.indices arr == ((Array.range . Array.bounds) arr)

prop_elems =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->
    let arr = Array.array b vs
    in Array.elems arr == [arr Array.! i | i <- Array.indices arr]

prop_assocs =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->
    let arr = Array.array b vs
    in Array.assocs arr == [(i, arr Array.! i) | i <- Array.indices arr]

prop_slashslash =
    forAll genBounds       $ \ (b :: (Int,Int))     ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)])  ->
    let arr = Array.array b vs
        us = []
    in arr Array.// us == Array.array (Array.bounds arr)
                          ([(i,arr Array.! i)
                            | i <- Array.indices arr \\ [i | (i,_) <- us]]
                             ++ us)
prop_accum =
    forAll genBounds          $ \ (b :: (Int,Int))    ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->

    forAll (genIVPs b 10)     $ \ (us :: [(Int,Int)]) ->
    forAll (choose (0,length us))
                           $ \ n ->
    let us' = take n us in
    forAll arbitrary       $ \ (fn :: Int -> Int -> Int) ->
    let arr = Array.array b vs
    in Array.accum fn arr us'
        == foldl (\a (i,v) -> a Array.// [(i,fn (a Array.! i) v)]) arr us'

prop_accumArray =
    forAll arbitrary          $ \ (f :: Int -> Int -> Int) ->
    forAll arbitrary          $ \ (z :: Int) ->
    forAll genBounds          $ \ (b :: (Int,Int))    ->
    forAll (genIVPs b 10)     $ \ (vs :: [(Int,Int)]) ->
    Array.accumArray f z b vs == Array.accum f
                (Array.array b [(i,z) | i <- Array.range b]) vs


same_arr :: (Eq b) => Array.Array Int b -> Array Int b -> Bool
same_arr a1 a2 = a == c && b == d
                 && all (\ n -> (a1 Array.! n) == (a2 ! n)) [a..b]
    where (a,b) = Array.bounds a1 :: (Int,Int)
          (c,d) = bounds a2 :: (Int,Int)

genBounds :: Gen (Int,Int)
genBounds = do m <- choose (0,20)
               n <- choose (minBound,maxBound-m)
               return (n,n+m-1)

genIVP :: Arbitrary a => (Int,Int) -> Gen (Int,a)
genIVP b = do { i <- choose b
              ; v <- arbitrary
              ; return (i,v)
              }

genIVPs :: Arbitrary a => (Int,Int) -> Int -> Gen [(Int,a)]
genIVPs b@(low,high) s
  = do { let is = [low..high]
       ; vs <- vector (length is)
       ; shuffleN s (zip is vs)
       }

shuffleN 0 xs = return xs
shuffleN n xs = shuffle xs >>= shuffleN (n - 1)

prop_id = forAll genBounds $ \ (b :: (Int,Int)) ->
          forAll (genIVPs b 10) $ \ (ivps :: [(Int,Int)])  ->
          label (show (ivps :: [(Int,Int)])) True

-- rift takes a list, split it (using an Int argument),
-- and then rifts together the split lists into one.
-- Think: rifting a pack of cards.
rift :: Int -> [a] -> [a]
rift n xs = comb (drop n xs) (take n xs)
   where
      comb (a:as) (b:bs) = a : b : comb as bs
      comb (a:as) []     = a : as
      comb []     (b:bs) = b : bs
      comb []     []     = []


prop_shuffle =
    forAll (shuffleN 10 [1..10::Int]) $ \ lst ->
    label (show lst) True

------------------------------------------------------------------------------

arr016 :: TestTree
arr016 = testGroup "arr016"
  [ testProperty "array" prop_array
  , testProperty "listArray" prop_listArray
  , testProperty "indicies" prop_indices
  , testProperty "elems" prop_elems
  , testProperty "assocs" prop_assocs
  , testProperty "slashslash" prop_slashslash
  , testProperty "accum" prop_accum
  , testProperty "accumArray" prop_accumArray ]


instance Show (a -> b) where { show _ = "<FN>" }

------------------------------------------------------------------------------

data Array a b = MkArray (a,a) (a -> b) deriving ()

array       :: (Ix a) => (a,a) -> [(a,b)] -> Array a b
array b ivs =
    if and [inRange b i | (i,_) <- ivs]
        then MkArray b
                     (\j -> case [v | (i,v) <- ivs, i == j] of
                            [v]   -> v
                            []    -> error "Array.!: \
                                           \undefined array element"
                            _     -> error "Array.!: \
                                           \multiply defined array element")
        else error "Array.array: out-of-range array association"

listArray             :: (Ix a) => (a,a) -> [b] -> Array a b
listArray b vs        =  array b (zipWith (\ a b -> (a,b)) (range b) vs)

(!)                   :: (Ix a) => Array a b -> a -> b
(!) (MkArray _ f)     =  f

bounds                :: (Ix a) => Array a b -> (a,a)
bounds (MkArray b _)  =  b

indices               :: (Ix a) => Array a b -> [a]
indices               =  range . bounds

elems                 :: (Ix a) => Array a b -> [b]
elems a               =  [a!i | i <- indices a]

assocs                :: (Ix a) => Array a b -> [(a,b)]
assocs a              =  [(i, a!i) | i <- indices a]

(//)                  :: (Ix a) => Array a b -> [(a,b)] -> Array a b
a // us               =  array (bounds a)
                            ([(i,a!i) | i <- indices a \\ [i | (i,_) <- us]]
                             ++ us)

accum                 :: (Ix a) => (b -> c -> b) -> Array a b -> [(a,c)]
                                   -> Array a b
accum f               =  foldl (\a (i,v) -> a // [(i,f (a!i) v)])

accumArray            :: (Ix a) => (b -> c -> b) -> b -> (a,a) -> [(a,c)]
                                   -> Array a b
accumArray f z b      =  accum f (array b [(i,z) | i <- range b])

ixmap                 :: (Ix a, Ix b) => (a,a) -> (a -> b) -> Array b c
                                         -> Array a c
ixmap b f a           = array b [(i, a ! f i) | i <- range b]

instance  (Ix a)          => Functor (Array a) where
    fmap fn (MkArray b f) =  MkArray b (fn . f)

instance  (Ix a, Eq b)  => Eq (Array a b)  where
    a == a'             =  assocs a == assocs a'

instance  (Ix a, Ord b) => Ord (Array a b)  where
    a <=  a'            =  assocs a <=  assocs a'

instance  (Ix a, Show a, Show b) => Show (Array a b)  where
    showsPrec p a = showParen (p > 9) (
                    showString "array " .
                    shows (bounds a) . showChar ' ' .
                    shows (assocs a)                  )

instance  (Ix a, Read a, Read b) => Read (Array a b)  where
    readsPrec p = readParen (p > 9)
           (\r -> [(array b as, u) | ("array",s) <- lex r,
                                     (b,t)       <- reads s,
                                     (as,u)      <- reads t   ])
--------------------------------------------------------------------

-- QuickCheck v.0.2
-- DRAFT implementation; last update 000104.
-- Koen Claessen, John Hughes.
-- This file represents work in progress, and might change at a later date.


--------------------------------------------------------------------
-- Generator


