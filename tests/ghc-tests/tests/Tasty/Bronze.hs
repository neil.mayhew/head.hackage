{-# LANGUAGE ScopedTypeVariables, BangPatterns #-}
module Tasty.Bronze where

import System.Exit
import Test.Tasty
import Test.Tasty.Silver
import qualified System.Process as PT
import Control.Exception
import System.IO
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as T
import System.Posix.Process.Internals
import System.IO.Temp


-- | Compares a given file with the output (exit code, stdout, stderr) of a program. Assumes
-- that the program output is utf8 encoded.
goldenVsOut
  :: TestName   -- ^ test name
  -> FilePath   -- ^ path to the golden file
  -> (Handle -> Handle -> IO ())
  -> TestTree
goldenVsOut name ref cmd =
  goldenVsAction name ref runProg printProcResult
  where runProg = wrapAction cmd

wrapAction :: (Handle -> Handle -> IO ()) -> IO (ExitCode, T.Text, T.Text)
wrapAction io =
  withSystemTempFile "stdout" $ \stdout_fp stdout_h -> do
    withSystemTempFile "stderr" $ \stderr_fp stderr_h -> do
      e_code <- catchExit (io stdout_h stderr_h)
      hClose stdout_h
      hClose stderr_h
      !std_o <- readFile stdout_fp
      !std_e <- readFile stderr_fp
      return (e_code, T.pack std_o, T.pack std_e)

catchExit :: IO () -> IO ExitCode
catchExit io = catch (ExitSuccess <$ io) (\(e :: ExitCode) -> return e)
