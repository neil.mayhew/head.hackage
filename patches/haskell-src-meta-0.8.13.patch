diff --git a/src/Language/Haskell/Meta/Syntax/Translate.hs b/src/Language/Haskell/Meta/Syntax/Translate.hs
index d4b45c4..7f2b062 100644
--- a/src/Language/Haskell/Meta/Syntax/Translate.hs
+++ b/src/Language/Haskell/Meta/Syntax/Translate.hs
@@ -44,6 +44,14 @@ class ToTyVars a where toTyVars :: a -> [TyVarBndr_ ()]
 class ToMaybeKind a where toMaybeKind :: a -> Maybe TH.Kind
 class ToInjectivityAnn a where toInjectivityAnn :: a -> TH.InjectivityAnn
 
+#if __GLASGOW_HASKELL__ < 909
+toArgPat :: ToPat a => a -> TH.Pat
+toArgPat = toPat
+#else
+toArgPat :: ToPat a => a -> TH.ArgPat
+toArgPat = TH.VisAP . toPat
+#endif
+
 type DerivClause = TH.DerivClause
 
 class ToDerivClauses a where toDerivClauses :: a -> [DerivClause]
@@ -267,7 +275,7 @@ instance ToExp (Exts.Exp l) where
   toExp (Exts.App _ e (Exts.TypeApp _ t)) = TH.AppTypeE (toExp e) (toType t)
   toExp (Exts.App _ e f)               = TH.AppE (toExp e) (toExp f)
   toExp (Exts.NegApp _ e)              = TH.AppE (TH.VarE 'negate) (toExp e)
-  toExp (Exts.Lambda _ ps e)           = TH.LamE (fmap toPat ps) (toExp e)
+  toExp (Exts.Lambda _ ps e)           = TH.LamE (fmap toArgPat ps) (toExp e)
   toExp (Exts.Let _ bs e)              = TH.LetE (toDecs bs) (toExp e)
   toExp (Exts.If _ a b c)              = TH.CondE (toExp a) (toExp b) (toExp c)
   toExp (Exts.MultiIf _ ifs)           = TH.MultiIfE (map toGuard ifs)
@@ -736,11 +744,11 @@ hsMatchesToFunD xs@(Exts.InfixMatch _ _ n _ _ _ : _) = TH.FunD (toName n) (fmap
 
 hsMatchToClause :: Exts.Match l -> TH.Clause
 hsMatchToClause (Exts.Match _ _ ps rhs bnds) = TH.Clause
-                                                (fmap toPat ps)
+                                                (fmap toArgPat ps)
                                                 (hsRhsToBody rhs)
                                                 (toDecs bnds)
 hsMatchToClause (Exts.InfixMatch _ p _ ps rhs bnds) = TH.Clause
-                                                        (fmap toPat (p:ps))
+                                                        (fmap toArgPat (p:ps))
                                                         (hsRhsToBody rhs)
                                                         (toDecs bnds)
 
@@ -798,8 +806,14 @@ instance ToDecs (Exts.Decl l) where
   toDecs (Exts.InfixDecl l assoc Nothing ops) =
       toDecs (Exts.InfixDecl l assoc (Just 9) ops)
   toDecs (Exts.InfixDecl _ assoc (Just fixity) ops) =
-    map (\op -> TH.InfixD (TH.Fixity fixity dir) (toName op)) ops
+    map mkFixitySig ops
    where
+    mkFixitySig =
+#if __GLASGOW_HASKELL__ < 909
+      \op -> TH.InfixD (TH.Fixity fixity dir) (toName op)
+#else
+      \op -> TH.InfixD (TH.Fixity fixity dir) TH.NoNamespaceSpecifier (toName op)
+#endif
     dir = case assoc of
       Exts.AssocNone _  -> TH.InfixN
       Exts.AssocLeft _  -> TH.InfixL
diff --git a/src/Language/Haskell/Meta/Utils.hs b/src/Language/Haskell/Meta/Utils.hs
index d9a0585..bd09e9d 100644
--- a/src/Language/Haskell/Meta/Utils.hs
+++ b/src/Language/Haskell/Meta/Utils.hs
@@ -320,7 +320,7 @@ fromDataConI (DataConI dConN ty _tyConN) =
   let n = arityT ty
   in replicateM n (newName "a")
       >>= \ns -> return (Just (LamE
-                    [Compat.conP dConN (fmap VarP ns)]
+                    [mkArgPat $ Compat.conP dConN (fmap (VarP) ns)]
 #if MIN_VERSION_template_haskell(2,16,0)
                     (TupE $ fmap (Just . VarE) ns)
 #else
@@ -334,7 +334,15 @@ fromTyConI (TyConI dec) = Just dec
 fromTyConI _            = Nothing
 
 mkFunD :: Name -> [Pat] -> Exp -> Dec
-mkFunD f xs e = FunD f [Clause xs (NormalB e) []]
+mkFunD f xs e = FunD f [Clause (map mkArgPat xs) (NormalB e) []] where
+
+#if __GLASGOW_HASKELL__ < 909
+mkArgPat :: Pat -> Pat
+mkArgPat = id
+#else
+mkArgPat :: Pat -> ArgPat
+mkArgPat = VisAP
+#endif
 
 mkClauseQ :: [PatQ] -> ExpQ -> ClauseQ
 mkClauseQ ps e = clause ps (normalB e) []
